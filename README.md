# README #

 

This repository contains the raw data and the rmarkdown document for reperforming the bioinformatic analysis.

+ The .Rmd contains the R mark down document with the code
+ The .html has been generated in RStudio from the .Rmd file.
+ The .txt file contain the raw data from the microarray experiment.
+ Supplementary table are in .csvformat.
+ analysis.RData contain all the variables saved in R.
+ dixonTADs_probeNames.csv contains the list of probes for each TAD
+ Addictive genes contains the list of genes involved in addiction.
+ all_variables_behaviour_more_samples and all_variables_behaviour_retaken contain behavioral data
 
The GEO reference is GSE100012.